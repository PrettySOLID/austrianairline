import java.util.Calendar;
import java.util.GregorianCalendar;
import java.text.SimpleDateFormat;

public class Angebot {
    private double regulaerpreis;
    private GregorianCalendar flugdatum;
    private String flugnummer;

    public Angebot(double regulaerpreis, GregorianCalendar flugdatum, String flugnummer) {
        this.regulaerpreis = regulaerpreis;
        this.flugdatum = flugdatum;
        this.flugnummer = flugnummer;
    }

    public double getRegulaerpreis() {
        return regulaerpreis;
    }

    public void setRegulaerpreis(double regulaerpreis) {
        this.regulaerpreis = regulaerpreis;
    }

    public GregorianCalendar getFlugdatum() {
        return flugdatum;
    }

    public void setFlugdatum(GregorianCalendar flugdatum) {
        this.flugdatum = flugdatum;
    }

    public String getFlugnummer() {
        return flugnummer;
    }

    public void setFlugnummer(String flugnummer) {
        this.flugnummer = flugnummer;
    }

    public double calculateReducedPrice() { //calculate reduced prices based on current month of flight
        double reducedPrice = 0.0;
        if(this.flugdatum.get(Calendar.MONTH) == Calendar.JANUARY || this.flugdatum.get(Calendar.MONTH) == Calendar.APRIL || this.flugdatum.get(Calendar.MONTH) == Calendar.OCTOBER) {
            RabattStrategie strat = new MaxiDiscount("maxi");
            reducedPrice = strat.getReducedPrice(this.regulaerpreis);
        }
        else if(this.flugdatum.get(Calendar.MONTH) == Calendar.FEBRUARY || this.flugdatum.get(Calendar.MONTH) == Calendar.MARCH) {
            RabattStrategie strat = new MidiDiscount("midi");
            reducedPrice = strat.getReducedPrice(this.regulaerpreis);
        }
        else {
            RabattStrategie strat = new ZeroDiscount("zero");
            reducedPrice = strat.getReducedPrice(this.regulaerpreis);
        }

        return reducedPrice;
    }

    public void anzeigen() { //print resulting prices to command line
        SimpleDateFormat date = new SimpleDateFormat("dd.MM.yyyy");

        System.out.println("Flight number:\t" + this.flugnummer);
        System.out.println("Date of Flight: " + date.format(this.flugdatum.getTime()));
        System.out.println("Original Price:\t" + this.regulaerpreis);
        System.out.println("Final Price:\t" + this.calculateReducedPrice() + "\n");
    }
}

import java.util.*;

public class App {
    public static void main(String[] args) {

        Angebot f1 = new Angebot(126.00, new GregorianCalendar(2019, Calendar.MARCH,16), "OS127"); //LOWW -> EDDF
        Angebot f2 = new Angebot(380.00, new GregorianCalendar(2020, Calendar.APRIL, 15), "OS335"); //LOWW -> ENGM
        Angebot f3 = new Angebot(158.00, new GregorianCalendar(2020, Calendar.APRIL, 18), "OS155"); //LOWW -> EDDL
        Angebot f4 = new Angebot(158.00, new GregorianCalendar(2020, Calendar.MAY, 3), "OS155"); //LOWW -> EDDL

        ArrayList<Angebot> flights = new ArrayList<>();

        flights.add(f1);
        flights.add(f2);
        flights.add(f3);
        flights.add(f4);

        for(Angebot f : flights) {
            f.anzeigen();
        }
    }
}
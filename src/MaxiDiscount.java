public class MaxiDiscount extends RabattStrategie {

    public MaxiDiscount(String bezeichnung) {
        super(bezeichnung);
    }

    public double getReducedPrice(double regularPrice) { //implement abstract method in RabattStrategie
        return regularPrice * 0.7;
    }
}
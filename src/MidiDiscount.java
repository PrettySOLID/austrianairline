public class MidiDiscount extends RabattStrategie {

    public MidiDiscount(String bezeichnung) {
        super(bezeichnung);
    }

    public double getReducedPrice(double regularPrice) { //implement abstract method in RabattStrategie
        return regularPrice * 0.85;
    }
}
public abstract class RabattStrategie {
    private String bezeichnung;

    public RabattStrategie(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public abstract double getReducedPrice(double regulaerpreis);

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }
}

public class ZeroDiscount extends RabattStrategie {

    public ZeroDiscount(String bezeichnung) {
        super(bezeichnung);
    }

    public double getReducedPrice(double regularPrice) { //implement abstract method in RabattStrategie
        return regularPrice;
    }
}